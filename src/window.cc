/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * analogdigitalconverter
 * Copyright (C) Szymon Sieciński 2012 <szymon.siecinski@gmail.com>
 * 
 * analogdigitalconverter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * analogdigitalconverter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "window.h"

Window::Window()
{
	win=0;
	input=0;
	im1=0;
	im2=0;
	scale=0;
	btn_about = 0;
}

void Window::init()
{
	/* For testing propose use the local (not installed) ui file */
	#define UI_FILE PACKAGE_DATA_DIR"/ui/analogdigitalconverter.ui" 
	/* #define UI_FILE "src/analogdigitalconverter.ui" */

	
	//Load the Glade file and instiate its widgets:
	Glib::RefPtr<Gtk::Builder> builder;
	try
	{
		builder = Gtk::Builder::create_from_file(UI_FILE);

		win = 0;
		builder->get_widget("main_window", win);
		builder->get_widget("scale1", scale);
		builder->get_widget("label1", input);
		builder->get_widget("image1", im1);
		builder->get_widget("image2", im2);
		builder->get_widget("image3", im3);
		builder->get_widget("button1",btn_about);

		if(scale)
		{
			scale->set_range (0,50);
			scale->set_value(0);
			scale->signal_value_changed().connect(sigc::mem_fun(*this, &Window::on_scale_change));
		}

		if(btn_about)
		{
			btn_about->signal_clicked().connect(sigc::mem_fun(*this, &Window::on_about_button_clicked));
		}
	}
	catch (const Glib::FileError & ex)
	{
		std::cerr << ex.what() << std::endl;
	}
}

Gtk::Window* Window::get_window()
{
	return win;
}

void Window::on_about_button_clicked()
{
	Gtk::AboutDialog about_dialog;
	about_dialog.set_program_name ("Przetwornik analogowo-cyfrowy");
	about_dialog.set_copyright ("© 2014 by Szymon Sieciński");
	about_dialog.set_version("Wersja 0.1");
	about_dialog.set_license_type(Gtk::LICENSE_GPL_3_0);

	about_dialog.run();
}

void Window::on_scale_change()
{
	Glib::ustring information = 
		Glib::ustring::compose("Napięcie na wejściu: %1 V",
	                                                   Glib::ustring::format(std::setprecision(2), scale->get_value()/10.0));
	
	if(scale->get_value()/10 >= 0 && scale->get_value()/10 < 2)
	{
		im1->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
		im2->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
		im3->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
	}

	if(scale->get_value()/10 >= 1 && scale->get_value()/10 < 2)
	{
		im1->set(Gtk::Stock::YES, Gtk::ICON_SIZE_BUTTON);
		im2->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
		im3->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
	}
	
	if(scale->get_value()/10 >= 2 && scale->get_value()/10 < 3)
	{
		im2->set(Gtk::Stock::YES, Gtk::ICON_SIZE_BUTTON);
		im1->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
		im3->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
	}
	
	if(scale->get_value()/10 >= 3 && scale->get_value()/10 < 4)
	{
		im1->set(Gtk::Stock::YES, Gtk::ICON_SIZE_BUTTON);
		im2->set(Gtk::Stock::YES, Gtk::ICON_SIZE_BUTTON);
		im3->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
	}

	if(scale->get_value()/10 >= 4 && scale->get_value()/10 < 5)
	{
		im1->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
		im2->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
		im3->set(Gtk::Stock::YES, Gtk::ICON_SIZE_BUTTON);
	}

	if(scale->get_value()/10 >= 5)
	{
		im1->set(Gtk::Stock::YES, Gtk::ICON_SIZE_BUTTON);
		im2->set(Gtk::Stock::NO, Gtk::ICON_SIZE_BUTTON);
		im3->set(Gtk::Stock::YES, Gtk::ICON_SIZE_BUTTON);
	}

	/* Zmienia opis napięcia */
	input->set_text(information);
}
