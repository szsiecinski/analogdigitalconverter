/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * analogdigitalconverter
 * Copyright (C) Szymon Sieciński 2012 <szymon.siecinski@gmail.com>
 * 
 * analogdigitalconverter is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * analogdigitalconverter is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _WINDOW_H_
#define _WINDOW_H_

#include <gtkmm.h>
#include <iostream>
#include <iomanip>

/**
 * Klasa implementująca zachowanie okna programu
 */
class Window: public Gtk::Window 
{
public:
	 /**
	  * Tworzy nowe okno programu
	  */
	Window();

	/**
	 * Inicjalizuje okno programu
	 */
	void init();

	/**
	 * Zwraca obiekt nowego okna
	 * @returns Wskaźnik na okno programu
	 */
	Gtk::Window* get_window();

protected:
	/**
	 * Implementuje zdarzenie zmiany wartości suwaka
	 */
	void on_scale_change();

	 /**
	  * Implementuje wciśnięcie przycisku "O programie" - wyświetlenie informacji o programie.
	  */
	void on_about_button_clicked();

private:
	Gtk::Window* win;
	Gtk::Label* input;
	Gtk::Image *im1, *im2, *im3;
	Gtk::Scale* scale;
	Gtk::Button* btn_about;
};

#endif // _WINDOW_H_
